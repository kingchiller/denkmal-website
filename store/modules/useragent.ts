import { Module } from 'vuex'
import { AllState } from 'store'

export { UseragentModule, UseragentState }

interface UseragentState {
  useragent: string | undefined
}

let UseragentModule: Module<UseragentState, AllState> = {
  namespaced: true,
  state: (): UseragentState => ({
    useragent: getUseragent(),
  }),
  getters: {
    isGeoUrlSupported: (state): boolean => {
      if (!state.useragent) {
        return true
      }
      if (/android/i.test(state.useragent)) {
        return true
      }
      return false
    },
  },
  actions: {
    async updateUseragent({ state }) {
      state.useragent = getUseragent()
    },
  },
}

function getUseragent(): string | undefined {
  let navigator = (global as any).navigator
  if (!navigator) {
    return undefined
  }
  return navigator.userAgent
}
