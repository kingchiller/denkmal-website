import { includes, uniq, without } from 'ramda'
import { getField, updateField } from 'vuex-map-fields'
import { Module } from 'vuex'
import { AllState } from 'store'

export { PreferenceModule, PreferencesState }

interface PreferencesState {
  favoriteVenues: string[]
  nightMode: string
  eventListSorting: string
}

let PreferenceModule: Module<PreferencesState, AllState> = {
  namespaced: true,
  state: (): PreferencesState => ({
    favoriteVenues: [],
    nightMode: 'auto',
    eventListSorting: 'alphabetically',
  }),
  mutations: {
    updateField,
    addFavoriteVenue(state, venueId: string) {
      state.favoriteVenues = uniq([...state.favoriteVenues, venueId])
    },
    removeFavoriteVenue(state, venueId: string) {
      state.favoriteVenues = without([venueId], state.favoriteVenues)
    },
  },
  getters: {
    getField,
    isFavoriteVenue: (state) => (id: string) => includes(id, state.favoriteVenues),
    isNightMode: (state, _getters, _rootState, rootGetters): boolean =>
      state.nightMode === 'on' || (state.nightMode === 'auto' && rootGetters['datetime/nowIsNight']),
  },
}
