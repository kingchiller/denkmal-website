import Vuex from 'vuex'
import { RootModule, RootState } from './root'
import { EventsModule, EventsState } from './modules/events'
import { PreferenceModule, PreferencesState } from './modules/userPreferences'
import { RegionsModule, RegionsState } from './modules/regions'
import { DatetimeModule, DatetimeState } from './modules/datetime'
import { UseragentModule, UseragentState } from '~/store/modules/useragent'

// For https://github.com/njam/nuxt-i18n-module/
interface I18nState {
  language: string
}

// Trick to have modules available on 'rootState' in other modules
export interface AllState extends RootState {
  events: EventsState
  regions: RegionsState
  userPreferences: PreferencesState
  useragent: UseragentState
  datetime: DatetimeState
  i18n: I18nState
}

const createStore = () => {
  return new Vuex.Store({
    state: RootModule.state as AllState,
    getters: RootModule.getters,
    actions: RootModule.actions,
    mutations: RootModule.mutations,
    modules: {
      events: EventsModule,
      regions: RegionsModule,
      userPreferences: PreferenceModule,
      useragent: UseragentModule,
      datetime: DatetimeModule,
    },
  })
}

export default createStore
