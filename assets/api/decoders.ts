import * as de from '@mojotech/json-type-validation'
import { Event, EventGenre, EventLink, Region, Venue } from '~/assets/types'
import { parseEventDateTime } from '../dateHelpers'

/**
 * These decoders convert from GraphQL API responses to internal types
 */

const eventLinkDecoder: de.Decoder<EventLink> = de.object({
  label: de.string(),
  url: de.string(),
})

const eventGenreDecoder: de.Decoder<EventGenre> = de.object({
  name: de.string(),
  category: de.object({
    color: de.oneOf(de.string(), de.constant(null)),
  }),
})

const venueDecoder: de.Decoder<Venue> = de.object({
  id: de.string(),
  name: de.string(),
  address: de.oneOf(de.string(), de.constant(null)),
  url: de.oneOf(de.string(), de.constant(null)),
  facebookPageId: de.oneOf(de.string(), de.constant(null)),
  latitude: de.oneOf(de.number(), de.constant(null)),
  longitude: de.oneOf(de.number(), de.constant(null)),
  region: de.object({
    slug: de.string(),
    dayOffset: de.number(),
    timeZone: de.string(),
  }),
})

const regionDecoder: de.Decoder<Region> = de.object({
  email: de.string(),
  slug: de.string(),
  name: de.string(),
  latitude: de.number(),
  longitude: de.number(),
  dayOffset: de.number(),
  timeZone: de.string(),
  suspendedUntil: de.oneOf(de.string(), de.constant(null)),
  venues: de.array(de.object({ name: de.string() })),
})

const eventDecoder: de.Decoder<Event> = de
  .object({
    id: de.string(),
    isPromoted: de.boolean(),
    description: de.string(),
    from: de.string(),
    until: de.oneOf(de.string(), de.constant(null)),
    hasTime: de.boolean(),
    eventDay: de.string(),
    links: de.array(eventLinkDecoder),
    genres: de.array(eventGenreDecoder),
    tags: de.array(),
    venue: venueDecoder,
  })
  .map((event) => {
    return {
      ...event,
      fromDT: parseEventDateTime(event.from),
      untilDT: event.until ? parseEventDateTime(event.until) : null,
    }
  })

export { regionDecoder, eventDecoder }
