import { range } from 'ramda'
import { DenkmalApi } from './api'
import { Event } from './types'
import { dateUrl } from './dateHelpers'
import { DenkmalConfig } from './denkmalConfig'
import { dateTimeNow } from './helpers'

export const generateDynamicRoutes = async (denkmalConfig: DenkmalConfig) => {
  const now = dateTimeNow()
  const datesRender = range(-10, 10).map((offset) => dateUrl(now.plus({ days: offset })))
  let api = new DenkmalApi(denkmalConfig.DENKMAL_API_URL)
  let regions = await api.fetchRegions()
  let eventDates = await api.fetchEvents(datesRender)

  let routes = []
  for (let region of regions) {
    // Region page
    routes.push({
      route: `/${region.slug}`,
    })

    // Date page
    for (let date of datesRender) {
      routes.push({
        route: `/${region.slug}/${date}`,
        payload: { eventDates },
      })
    }

    // Event page
    let eventsFlat: Event[] = eventDates
      .filter((d) => d.region === region.slug)
      .reduce((acc: Event[], d) => acc.concat(Object.values(d.events)), [])
    for (let event of eventsFlat) {
      routes.push({
        route: `/${region.slug}/${event.eventDay}/${event.id}`,
        payload: { event },
      })
    }
  }

  return routes
}
