import { DateTime } from 'luxon'

const isStaticGenerate = process.static && process.server

const isModifierKey = (event: KeyboardEvent): boolean =>
  event.altKey || event.ctrlKey || event.metaKey || event.shiftKey

function dateTimeNow(): DateTime {
  if (!!process.env.DENKMAL_MOCK_NOW) {
    return DateTime.fromISO(process.env.DENKMAL_MOCK_NOW)
  }
  return DateTime.local()
}

export { isStaticGenerate, isModifierKey, dateTimeNow }
