import glob from 'glob'
import { DateTime } from 'luxon'

function dateTimeNow() {
  if (!!process.env.DENKMAL_MOCK_NOW) {
    return DateTime.fromISO(process.env.DENKMAL_MOCK_NOW)
  }
  return DateTime.local()
}

function todayDate() {
  let dayOffset = 5
  return dateTimeNow().minus({ hours: dayOffset })
}

/**
 * @param {string} regionName
 * @returns {{region: string, date: string, eventId: string}[]}
 */
function getEvents(regionName) {
  let webDir = __dirname + '/../../dist'
  let paths = glob.sync(`${webDir}/${regionName}/*/*/index.html`)
  return paths.map((path) => {
    let match = path.match(/\/([^\/]+)\/([^\/]+)\/([^\/]+)\/index.html$/)
    if (!match) {
      throw new Error(`Cannot parse event path '${path}'`)
    }
    return { region: match[1], date: match[2], eventId: match[3] }
  })
}

/**
 * @param {string} regionName
 * @param {function} [filter]
 * @returns {{region: string, date: string, eventId: string}}
 */
function getEvent(regionName, filter) {
  let events = getEvents(regionName)
  if (filter) {
    events = events.filter(filter)
  }
  let event = events[0]
  if (!event) {
    throw new Error('Cannot find event to use for test')
  }
  return event
}

/**
 * Returns the URL of the denkmal backend API, which can be overridden
 * using the env var `DENKMAL_API_URL`.
 * @returns {string}
 */
function getDenkmalApiUrl() {
  return process.env.DENKMAL_API_URL || 'https://api.denkmal.org/graphql'
}

export { todayDate, getEvents, getEvent, getDenkmalApiUrl }
