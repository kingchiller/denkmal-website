import Vue from 'vue'
import Ripple from 'vue-ripple-directive'

Ripple.color = 'rgba(0, 0, 0, 0.05)'
Vue.directive('ripple', Ripple)
