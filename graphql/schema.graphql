# This file was generated based on ".graphqlconfig". Do not edit manually.

schema {
  query: Query
  mutation: Mutation
}

type Authentication {
  token: String
  user: User
}

type Badges {
  pendingEventsCount: Int!
  pendingGenresCount: Int!
  pendingVenuesCount: Int!
}

type Event {
  createdAt: Date!
  description: String!
  eventDay: EventDay!
  from: Date!
  genres: [Genre]!
  hasTime: Boolean!
  id: UUID!
  isHidden: Boolean!
  isPromoted: Boolean!
  isReviewPending: Boolean!
  links: [EventLink]!
  regionId: UUID!
  sourceIdentifier: String
  sourceType: SourceType!
  sourceUrl: URL
  tags: [String]!
  until: Date
  variants: [EventVariant]!
  venue: Venue!
  venueId: UUID!
}

type EventLink {
  label: String!
  url: URL!
}

type EventVariant {
  createdAt: Date!
  description: String!
  from: Date!
  genres: [Genre]!
  hasTime: Boolean!
  id: UUID!
  isReviewPending: Boolean!
  links: [EventLink]!
  tags: [String]!
  until: Date
}

type EventsList {
  count: Int!
  events: [Event]!
}

type Genre {
  category: GenreCategory!
  categoryId: UUID!
  createdAt: Date!
  id: UUID!
  isReviewPending: Boolean!
  name: String!
  updatedAt: Date!
}

type GenreCategoriesList {
  count: Int!
  genreCategories: [GenreCategory]!
}

type GenreCategory {
  color: String
  createdAt: Date!
  genres: [Genre]!
  id: UUID!
  name: String!
  regionId: UUID!
  updatedAt: Date!
}

type GenresList {
  count: Int!
  genres: [Genre]!
}

type Mutation {
  addEvent(description: String!, from: Date!, genres: [UUID], hasTime: Boolean, id: UUID, isHidden: Boolean, isPromoted: Boolean, links: [EventLinkInput], tags: [String], until: Date, venueId: UUID!): Event
  addGenre(genreCategoryId: UUID!, id: UUID, name: String!): Genre
  addGenreCategory(color: String, id: UUID, name: String!, regionId: UUID!): GenreCategory
  addRegion(dayOffset: Float, email: String!, id: UUID, latitude: Float!, longitude: Float!, name: String!, slug: String!, suspendedUntil: Date, timeZone: String!): Region
  addScraperEvents(events: [ScraperEventInput]!): [Boolean]
  addUser(accessLevel: AccessLevel, email: String!, id: UUID, loginKey: String, name: String!, password: password!, regionId: UUID): User
  addVenue(address: String, email: String, facebookPageId: String, id: UUID, ignoreScraper: Boolean, isReviewPending: Boolean, isSuspended: Boolean, latitude: Float, longitude: Float, name: String!, regionId: UUID!, twitter: String, url: URL): Venue
  changeGenre(genreCategoryId: UUID, id: UUID, name: String): Genre
  changeGenreCategory(color: String, id: UUID!, name: String): GenreCategory
  changeRegion(dayOffset: Float, email: String, id: UUID!, latitude: Float, longitude: Float, name: String, slug: String, suspendedUntil: Date, timeZone: String): Region
  changeUser(accessLevel: AccessLevel, email: String, id: UUID!, loginKey: String, name: String, password: password, regionId: UUID): User
  changeVenue(address: String, aliases: [String!], email: String, facebookPageId: String, id: UUID!, ignoreScraper: Boolean, isReviewPending: Boolean, isSuspended: Boolean, latitude: Float, longitude: Float, name: String, twitter: String, url: URL): Venue
  deleteGenre(id: UUID!): Boolean
  deleteGenreCategory(id: UUID!): Boolean
  deleteRegion(id: UUID!): Boolean
  deleteUser(id: UUID!): Boolean
  deleteVenue(id: UUID!): Boolean
  hideEvent(hide: Boolean!, id: UUID!): Event
  mergeVenue(fromId: UUID!, toId: UUID!): Venue
  promoteEvent(id: UUID!, promote: Boolean!): Event
  reviewEvent(description: String, from: Date, genres: [UUID], hasTime: Boolean, id: UUID!, isHidden: Boolean, isPromoted: Boolean, links: [EventLinkInput], tags: [String], until: Date, venueId: UUID): Event
  suggestEvent(description: String!, from: Date!, genres: [String], hasTime: Boolean, id: UUID, links: [EventLinkInput], regionSlug: String!, until: Date, venueName: String!): Event
}

type Query {
  authenticate(email: String!, password: String!): Authentication
  event(id: UUID!): Event
  genre(id: UUID!): Genre
  genreCategory(id: UUID): GenreCategory
  genres(ids: [UUID]!): [Genre]!
  region(id: UUID, slug: String): Region
  regions: [Region]
  regionsList(listOptions: ListOptionsInput): RegionsList!
  user(id: UUID!): User
  usersList(listOptions: ListOptionsInput): UsersList!
  venue(id: UUID!): Venue
}

type Region {
  badges: Badges!
  createdAt: Date!
  dayOffset: Float!
  email: String!
  events(eventDays: [EventDay!], withHidden: Boolean): [[Event]]
  eventsList(listOptions: ListOptionsInput, search: String): EventsList!
  genreCategoriesList(listOptions: ListOptionsInput): GenreCategoriesList!
  genresList(ignoreUnknown: Boolean, listOptions: ListOptionsInput, search: String): GenresList!
  id: UUID!
  latitude: Float!
  longitude: Float!
  name: String!
  slug: String!
  suspendedUntil: Date
  timeZone: String!
  updatedAt: Date!
  venues: [Venue]!
  venuesList(listOptions: ListOptionsInput, search: String): VenuesList!
}

type RegionsList {
  count: Int!
  regions: [Region]!
}

type User {
  accessLevel: AccessLevel!
  email: String!
  id: UUID!
  loginKey: String!
  name: String!
  region: Region
  regionId: UUID
}

type UsersList {
  count: Int!
  users: [User]!
}

type Venue {
  address: String
  aliases: [VenueAlias]
  createdAt: Date!
  email: String
  events: [Event]
  facebookPageId: String
  id: UUID!
  ignoreScraper: Boolean!
  isReviewPending: Boolean!
  isSuspended: Boolean!
  latitude: Float
  longitude: Float
  name: String!
  region: Region!
  regionId: UUID!
  twitter: String
  updatedAt: Date!
  url: String
}

type VenueAlias {
  id: UUID!
  name: String!
}

type VenuesList {
  count: Int!
  venues: [Venue]!
}

enum AccessLevel {
  Admin
  Regional
  Scraper
}

enum CacheControlScope {
  PRIVATE
  PUBLIC
}

enum SourceType {
  Admin
  Migration
  Scraper
  Suggestion
}

input EventLinkInput {
  label: String!
  url: URL!
}

input ListOptionsInput {
  pagination: PaginationInput
  sort: SortInput
}

input PaginationInput {
  page: Int!
  perPage: Int!
}

input ScraperEventInput {
  description: String!
  from: Date!
  genres: [String]
  hasTime: Boolean
  links: [EventLinkInput]
  regionId: UUID!
  sourceIdentifier: String!
  sourcePriority: Int
  sourceUrl: URL!
  until: Date
  venueName: String!
}

input SortInput {
  ascending: Boolean
  field: String!
}


"An RFC 4122 compatible UUID String"
scalar UUID

"ISO 8601 date string"
scalar Date

"URL string"
scalar URL

"Date with format: YYYY-MM-DD"
scalar EventDay

"password string with at least 8 characters"
scalar password

"The `Upload` scalar type represents a file upload."
scalar Upload
